package com.weixing.core.message.req;

/**
 * 文本消息
 * 
 * @author fudongwei
 * @date 2015-01-21
 */

public class TextMessage extends BaseMessage {
	// 消息内容
	private String Content;

	public String getContent() {
		return Content;
	}

	public void setContent(String content) {
		Content = content;
	}
}
