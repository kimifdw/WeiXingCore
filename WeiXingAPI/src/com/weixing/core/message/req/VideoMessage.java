package com.weixing.core.message.req;

/**
 * 视频消息
 * 
 * @author fudongwei
 * @date 2015-01-21
 */

public class VideoMessage extends BaseMessage {
	// 视频消息ID
	private String MediaId;
	// 视频消息缩略图ID
	private String ThumbMediaId;

	public String getMediaId() {
		return MediaId;
	}

	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}

	public String getThumbMediaId() {
		return ThumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		ThumbMediaId = thumbMediaId;
	}
}
