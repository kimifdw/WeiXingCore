package com.weixing.core.message.resp;

/**
 * 回复视频消息
 * 
 * @author fudongwei
 * @date 2015-01-21
 */

public class VideoMessage extends BaseMessage {
	// 通过上传多媒体文件，得到的id
	private String MediaId;
	// 视频消息的标题
	private String Title;
	// 视频消息的描述
	private String Descrition;

	public String getMediaId() {
		return MediaId;
	}

	public void setMediaId(String mediaId) {
		MediaId = mediaId;
	}

	public String getTitle() {
		return null == Title ? "" : Title;
	}

	public void setTitle(String title) {
		Title = title;
	}

	public String getDescrition() {
		return null == Descrition ? "" : Descrition;
	}

	public void setDescrition(String descrition) {
		Descrition = descrition;
	}

}
