package com.weixing.core.util;

/**
 * 
 * @author Administrator
 *
 */

public interface SignUtil {

	// 与接口配置信息中的Token要一致
	public static final String token = "fuyu1108";

	/**
	 * 验证签名
	 * 
	 * @param signature
	 * @param timestamp
	 * @param nonce
	 * @return
	 */
	boolean checkSignature(String signature, String timestamp, String nonce);

}
