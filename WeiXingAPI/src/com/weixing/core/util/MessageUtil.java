package com.weixing.core.util;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import com.weixing.core.message.resp.ImageMessage;
import com.weixing.core.message.resp.MusicMessage;
import com.weixing.core.message.resp.NewsMessage;
import com.weixing.core.message.resp.TextMessage;
import com.weixing.core.message.resp.VideoMessage;
import com.weixing.core.message.resp.VoiceMessage;

/**
 * 消息工具类
 * 
 * @author fudongwei
 * @date 2015-01-21
 */

public interface MessageUtil {

	/**
	 * 返回消息类型：文本
	 */
	public static final String RESP_MESSAGE_TYPE_TEXT = "text";

	/**
	 * 返回消息类型：图片
	 */
	public static final String RESP_MESSAGE_TYPE_IMAGE = "image";

	/**
	 * 返回消息类型：音乐
	 */
	public static final String RESP_MESSAGE_TYPE_MUSIC = "music";

	/**
	 * 返回消息类型：图文
	 */
	public static final String RESP_MESSAGE_TYPE_NEWS = "news";

	/**
	 * 返回消息类型：语音
	 */
	public static final String RESP_MESSAGE_TYPE_VOICE = "voice";

	/**
	 * 返回消息类型：视频
	 */
	public static final String RESP_MESSAGE_TYPE_VIDEO = "video";

	/**
	 * 请求消息类型：文本
	 */
	public static final String REQ_MESSAGE_TYPE_TEXT = "text";

	/**
	 * 请求消息类型：图片
	 */
	public static final String REQ_MESSAGE_TYPE_IMAGE = "image";

	/**
	 * 请求消息类型：链接
	 */
	public static final String REQ_MESSAGE_TYPE_LINK = "link";

	/**
	 * 请求消息类型：地理位置
	 */
	public static final String REQ_MESSAGE_TYPE_LOCATION = "location";

	/**
	 * 请求消息类型：音频
	 */
	public static final String REQ_MESSAGE_TYPE_VOICE = "voice";

	/**
	 * 请求消息类型：视频
	 */
	public static final String REQ_MESSAGE_TYPE_VIDEO = "video";

	/**
	 * 请求消息类型：推送
	 */
	public static final String REQ_MESSAGE_TYPE_EVENT = "event";

	/**
	 * 事件类型：subscribe(订阅)
	 */
	public static final String EVENT_TYPE_SUBSCRIBE = "SUBSCRIBE";

	/**
	 * 事件类型：unsubscribe(取消订阅)
	 */
	public static final String EVENT_TYPE_UNSUBSCRIBE = "UNSUBSCRIBE";

	/**
	 * 事件类型：扫描带参数二维码事件
	 */
	public static final String EVENT_TYPE_SCAN = "SCAN";

	/**
	 * 事件类型：上报地理位置事件
	 */
	public static final String EVENT_TYPE_LOCATION = "LOCATION";

	/**
	 * 事件类型：CLICK(自定义菜单点击事件)
	 */
	public static final String EVENT_TYPE_CLICK = "CLICK";

	/**
	 * 文本消息对象转换成xml【发送消息】
	 * 
	 * @param textMessage
	 *            文本消息对象
	 * @return xml
	 */
	String textMessageToXml(TextMessage textMessage);

	/**
	 * 图片消息对象转换成xml
	 */
	String imageMessageToXml(ImageMessage imageMessage);

	/**
	 * 音乐消息对象转换成xml
	 * 
	 * @param musicMessage
	 *            音乐消息对象
	 * @return xml
	 */
	String musicMessageToXml(MusicMessage musicMessage);

	/**
	 * 视频消息对象转换成xml
	 * 
	 * @param videoMessage
	 * @return
	 */
	String videoMessageToXml(VideoMessage videoMessage);

	/**
	 * 将语音对象转换成xml
	 * 
	 * @param voiceMessage
	 * @return
	 */
	String voiceMessageToXml(VoiceMessage voiceMessage);

	/**
	 * 图文消息对象转换成xml
	 * 
	 * @param newsMessage
	 *            图文消息对象
	 * @return xml
	 */
	String newsMessageToXml(NewsMessage newsMessage);

	/**
	 * 解析微信发来的请求（XML）[接收消息] 将xml转化成hashmap
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	Map<String, String> parseXml(HttpServletRequest request);

	/**
	 * 判断是否是QQ表情
	 * 
	 * @param content
	 * @return
	 */
	boolean isQqFace(String content);

	/**
	 * 将微信消息中的CreateTime转换成标准格式的时间（yyyy-MM-dd HH:mm:ss）
	 * 
	 * @param createTime
	 *            消息创建时间
	 * @return
	 */
	String formatTime(String createTime);

	/**
	 * emoji表情转换(hex -> utf-16) 将hexEmoji从U+换成0x
	 * 
	 * @param hexEmoji
	 * @return
	 */
	String emoji(int hexEmoji);
}
