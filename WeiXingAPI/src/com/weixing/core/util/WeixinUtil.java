package com.weixing.core.util;

import com.alibaba.fastjson.JSONObject;

/**
 * 公众平台通用接口工具
 * 
 * @author fudongwei
 * @date 2015-01-21
 */
public interface WeixinUtil {

	/**
	 * 发起https请求并获取结果
	 * 
	 * @param requestUrl
	 *            请求地址
	 * @param requestMethod
	 *            请求方式（GET、POST）
	 * @param outputStr
	 *            提交的数据
	 * @return JSONObject(通过JSONObject.get(key)的方式获取json对象的属性值)
	 */
	JSONObject httpRequest(String requestUrl, String requestMethod,
			String outputStr);

	/**
	 * 发起http get请求获取网页源代码
	 * 
	 * @param requestUrl
	 * @return
	 */
	String httpRequest(String requestUrl);

}
