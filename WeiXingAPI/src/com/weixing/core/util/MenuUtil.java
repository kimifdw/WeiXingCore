package com.weixing.core.util;

import com.weixing.core.entity.Menu;

public interface MenuUtil {
	/**
	 * 菜单创建URL
	 */
	public static String menu_create_url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
	/**
	 * 菜单查询URL
	 */
	public static String menu_search_url = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=ACCESS_TOKEN";
	/**
	 * 菜单删除URL
	 */
	public static String menu_delete_url = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN";

	/**
	 * 创建菜单
	 */
	int CreateMenu(Menu menu, String accessToken);

	/**
	 * 查询菜单
	 */
	Menu SearchMenu(String accessToken);

	/**
	 * 删除菜单
	 */
	int DeleteMenu(Menu menu, String accessToken);
}
