package com.weixing.core.util;

public class StringUtil {

	/**
	 * 处理JSON 处理前：{"weatherinfo":{"city":"宁波","cityid":"101210401"}}
	 * 处理后：{"city":"宁波","cityid":"101210401"}
	 */
	public String DoJsonToObject(String jsonStr) {
		String result = "";
		if (!"".equals(jsonStr)) {
			int index = jsonStr.indexOf(":");
			result = jsonStr.substring(index + 1, jsonStr.length() - 1);
		}
		return result;
	}
}
