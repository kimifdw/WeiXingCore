package com.weixing.core.service.util;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.weixing.core.entity.ContentKeyWord;
import com.weixing.core.message.resp.TextMessage;
import com.weixing.core.service.CoreService;
import com.weixing.core.service.TodayInHistoryService;
import com.weixing.core.service.WeatherService;
import com.weixing.core.util.MessageUtil;
import com.weixing.core.util.impl.MessageUtilForImpl;

/**
 * 微信服务入口实现
 * 
 * @author fudongwei
 * @Date 2015-01-24
 * @version 1.0
 */
public class CoreServiceForImpl implements CoreService {

	@Override
	public String processRequest(HttpServletRequest request) {

		String respMessage = "";
		// 默认返回的文本消息内容
		String respContent = "请求处理异常，请稍候尝试！";
		MessageUtil messageUtil = new MessageUtilForImpl();
		TextMessage textMessage = new TextMessage();
		TodayInHistoryService todayHistoryService = new TodayInHistoryServiceForImpl();
		WeatherService weatherService = new WeatherServiceForImpl();
		// xml请求解析
		Map<String, String> requestMap = messageUtil.parseXml(request);

		// 发送方帐号（open_id）
		String fromUserName = requestMap.get("FromUserName");
		// 公众帐号
		String toUserName = requestMap.get("ToUserName");
		// 消息类型
		String msgType = requestMap.get("MsgType");
		// 消息内容
		String content = requestMap.get("Content");

		// 回复文本消息
		textMessage.setToUserName(fromUserName);
		textMessage.setFromUserName(toUserName);
		// 因为new Date().getTime()得到的是毫秒数而微信createTime是秒数
		textMessage.setCreateTime(new Date().getTime() / 1000L);
		textMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_TEXT);
		textMessage.setFuncFlag(0);

		// 文本消息
		if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_TEXT)) {
			if (content.contains("?")) {
				respContent = getHelperMessage();
			} else {
				if (content.contains(ContentKeyWord.TodayHistory)) {
					respContent = todayHistoryService.getTodayInHistoryInfo();
				} else if (content.contains(ContentKeyWord.TodayWeather)) {
					String keyWord = content.replace("天气+", "");
					respContent = weatherService.getWeatherInfo(keyWord);
				} else {
					respContent = "功能正在开发中。。。";
				}
			}
		}
		// 图片消息
		else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_IMAGE)) {
			respContent = "您发送的是图片消息！";
		}
		// 地理位置消息
		else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_LOCATION)) {
			respContent = "您发送的是地理位置消息！";
		}
		// 链接消息
		else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_LINK)) {
			respContent = "您发送的是链接消息！";
		}
		// 音频消息
		else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_VOICE)) {
			respContent = "您发送的是音频消息！";
		}
		// 视频消息
		else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_VIDEO)) {
			respContent = "您发送的是视频消息！";
		}
		// 事件推送
		else if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_EVENT)) {
			// 事件类型
			String eventType = requestMap.get("Event");
			// 订阅
			if (eventType.equals(MessageUtil.EVENT_TYPE_SUBSCRIBE)) {
				respContent = "谢谢您的关注！";
			}
			// 取消订阅
			else if (eventType.equals(MessageUtil.EVENT_TYPE_UNSUBSCRIBE)) {
				// TODO 取消订阅后用户再收不到公众号发送的消息，因此不需要回复消息
			}
			// 自定义菜单点击事件
			else if (eventType.equals(MessageUtil.EVENT_TYPE_CLICK)) {
				// TODO 自定义菜单权没有开放，暂不处理该类消息
				// 1.获取与自定义菜单KEY对应的KEYID
				// 2.设置的跳转URL
				// String eventKey = requestMap.get("EventKey");
			}
			// 自定义扫描带参数二维码事件
			else if (eventType.equals(MessageUtil.EVENT_TYPE_SCAN)) {
				// TODO
				// 创建二维码时的二维码scene_id
				// int eventKey =
				// Integer.parseInt(requestMap.get("EventKey"));
				// 二维码的ticket，可用来换取二维码图片
				// String ticket = requestMap.get("Ticket");
			}
			// 自定义上报地理位置
			else if (eventType.equals(MessageUtil.EVENT_TYPE_LOCATION)) {
				// TODO 获取经纬度
				// 纬度
				// double latitude =
				// Double.parseDouble(requestMap.get("Latitude"));
				// 经度
				// double longitude =
				// Double.parseDouble(requestMap.get("Longitude"));
				// 地理位置
				// double precision =
				// Double.parseDouble(requestMap.get("Precision"));
			}
		}

		textMessage.setContent(respContent);
		// 发送消息
		respMessage = messageUtil.textMessageToXml(textMessage);
		return respMessage;
	}

	/**
	 * 帮助提示信息
	 * 
	 * @return
	 */
	private String getHelperMessage() {
		StringBuilder buffer = new StringBuilder();
		buffer.append("傅虞无敌使用指南").append("\n\n");
		buffer.append("    1.历史上的今天").append("\n");
		buffer.append("    2.城市天气").append("\n");
		buffer.append("    3.待开发中").append("\n\n");
		buffer.append("使用示例：").append("\n");
		buffer.append("    历史上的今天").append("\n");
		buffer.append("    天气+宁波").append("\n");
		buffer.append("回复“?”显示主菜单");
		return buffer.toString();
	}
}
