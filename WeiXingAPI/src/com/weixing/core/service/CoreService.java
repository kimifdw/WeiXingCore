package com.weixing.core.service;

import javax.servlet.http.HttpServletRequest;

/**
 * 核心服务类
 * 
 * @author fudongwei
 * @date 2015-01-24
 */
public interface CoreService {

	/**
	 * 处理微信发来的请求 即接收微信服务器发来的消息经处理后将其转化为可发送的字符串类型（XML）
	 * 
	 * @param request
	 * @return
	 */
	String processRequest(HttpServletRequest request);
}
