package com.weixing.core.service;

public interface WeatherService {

	/**
	 * 天气url
	 */
	public static final String WEATHERURL = "http://www.weather.com.cn/data/cityinfo/CITYID.html";
	public static final String WEATHERNEWURL = "http://m.weather.com.cn/mweather/CITYID.shtml";
	public static final String WEATHERFIVEURL = "http://api.k780.com:88/?app=weather.future&weaid=WEAID&appkey=12901&sign=924d3dc9d4926ab6c20fdb22b1f1bf79&format=json";
	/**
	 * 城市url
	 */
	public static final String WEATHERCITYURL = "http://api.k780.com:88/?app=weather.city&format=json";

	/**
	 * 获取某个城市的天气情况
	 * 
	 * @param city
	 *            城市名称
	 * @return
	 */
	String getWeatherInfo(String city);

}
