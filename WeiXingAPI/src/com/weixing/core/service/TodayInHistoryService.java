package com.weixing.core.service;

/**
 * 历史上的今天查询服务
 * 
 * @author fudongwei
 * @date 2015-01-25
 * 
 */
public interface TodayInHistoryService {

	/**
	 * 获取历史上的今天
	 * 
	 * @return
	 */
	String getTodayInHistoryInfo();
}
