package com.weixing.core.entity;

/**
 * 微信返回的错误代码
 * 
 * @author fudongwei
 * @Date 2015-01-22
 */

public class Error {
	// 错误代码
	private int code;
	// 错误代码描述
	private String msg;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
