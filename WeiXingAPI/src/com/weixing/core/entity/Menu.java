package com.weixing.core.entity;

/**
 * 菜单
 * 
 * @author fudongwei
 * @date 2015-01-21
 */

public class Menu {
	private Button[] button;

	public Button[] getButton() {
		return button;
	}

	public void setButton(Button[] button) {
		this.button = button;
	}
}
