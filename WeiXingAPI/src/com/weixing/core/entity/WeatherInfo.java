package com.weixing.core.entity;

/**
 * 天气信息
 * 
 * @author fudongwei
 * @date 2015-01-25
 */

public class WeatherInfo {
	// 城市名称
	private String city;
	// 日期
	private String days;
	// 星期几
	private String week;
	// 温度
	private String temperature;
	// 天气状况
	private String weather;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDays() {
		return days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public String getWeek() {
		return week;
	}

	public void setWeek(String week) {
		this.week = week;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public String getWeather() {
		return weather;
	}

	public void setWeather(String weather) {
		this.weather = weather;
	}
}
