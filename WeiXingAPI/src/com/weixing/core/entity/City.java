package com.weixing.core.entity;

/**
 * 城市名称
 * 
 * @author fudongwei
 * @Date 2015-01-24
 */

public class City {
	// 唯一id
	private int weaid;
	// 城市名称
	private String citynm;
	// 城市拼音
	private String cityno;
	// 城市id
	private String cityid;

	public int getWeaid() {
		return weaid;
	}

	public void setWeaid(int weaid) {
		this.weaid = weaid;
	}

	public String getCitynm() {
		return citynm;
	}

	public void setCitynm(String citynm) {
		this.citynm = citynm;
	}

	public String getCityno() {
		return cityno;
	}

	public void setCityno(String cityno) {
		this.cityno = cityno;
	}

	public String getCityid() {
		return cityid;
	}

	public void setCityid(String cityid) {
		this.cityid = cityid;
	}

}
