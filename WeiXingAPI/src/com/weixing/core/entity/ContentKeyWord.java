package com.weixing.core.entity;

/**
 * 关键字
 * 
 * @author fudongwei
 * @Date 2015-01-23
 *
 */
public interface ContentKeyWord {
	/**
	 * 历史上的今天
	 */
	public static final String TodayHistory = "历史上的今天";

	/**
	 * 今天的天气
	 */
	public static final String TodayWeather = "天气";
}
